---
layout: default
title: Setup
permalink: /setup/
---

# My Setup

## Hardware

I work remotely for GitLab, from the comfort of my home. So, I don't have
separate setups at my home and my "office". :)

Most of my time is spent on my work laptop - a Lenovo Thinkpad x260 with 16GB RAM and
512GB SSD. I used to have a personal laptop, a Lenovo Thinkpad E230, which can
be called "broken" nowadays.

I am using an LG 27" widescreen as my main monitor, and use the laptop screen
rarely (it is most of the time closed). For input, most of my stuff comes from
Logitech - A Logitech G613 wireless mechanical keyboard, Logitech M235 wireless
mouse, and a Logitech C270 webcam. I use a Sennheiser HD 4.40-BT bluetooth
headphones for work, an Anker Soundbuds Slim+ bluetooth headphones while I am
travelling, and a Sennheiser CX 180 Street II headphone while I am at home
listening music or watching movies at home.

I am a self-proclaimed Bibliphile. Even though I prefer reading dead-tree books
more, I own a Kindle Paperwhite 7th edition. For some reason, I never had any
interest in jailbreaking it.

I have a Raspberry Pi 3 model B that I use as my "torrent downloader", with a
custom script I have written to fetch latest episodes of my favorite TV series.
In addition to that, I am using an old Compaq Presario CQ3000 as a local server
with three Western Digital external hard drives.

I currently use a Mi A1 as my primary phone, and an old Moto G4 as my
play-games-while-in-the-toilet phone.

## Software

I use only GNU/Linux in my computers. I used to hop distros while I was in
college and have used Ubuntu, Linux Mint, Arch Linux, Fedora, and Manjaro Linux
at some point in the past. Currently, all my machines run Debian GNU/Linux.
Server runs latest stable, Pi runs Raspbian, and my work machine runs Sid.

I use i3 as my window manager, for which my widescreen monitor is a huge help. I
am currently using Zsh as my terminal shell with Pure promt (tried oh-my-zsh,
loved it, later realized I am not using 90% of it and that most of it is
essentially bloat, and then got rid of it). I use Vim as my main editor (don't
konw how to operate Emacs and have no plan to learn how to), and occasionally
use gedit when my typing involves Malayalam.

Firefox is my main browser - both my mobile and computer. I have Google Chrome
also installed on my laptop to test if stuff is broken only in Firefox (it
happens more often than you think). Thunderbird and Mutt handles my mail,
Hexchat for IRC and Rambox for almost everything else (GMail, Slack, Whatsapp,
Tweetdeck, Calendar, Mastodon, Mattermost etc.). I also use Telegram Desktop and
Riot Desktop (for Matrix) for instant messaging.

My server runs Jellyfin as a media server and I use its webapp as well as mobile
app to watch my media. I also spend quite a lot of time on Youtube these days
(a variety of stuff that I can see Youtube's suggestion algorithm going haywire
deciding what to suggest to me).

Other tools I find myself using almost regularly are GIMP - mostly due to Indian
universities' need for "passport size photo less than 150Kb" and my relatives
having no clue how to do it, and Inkscape - to get SVGs of various logos and
stuff.
