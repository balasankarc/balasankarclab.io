---
layout: literature_post
status: publish
permalink: /literature/:title.html
published: true
title: "എന്നും തനി"
author:
  display_name: "ബാലശങ്കർ സി"
  login: admin
  email: c.balasankar@gmail.com
  url: ''
date: '2013-08-10 23:08:13 -0500'
categories:
- Literature
tags: []
---
<p><span style="float: left; font-size: 700%; line-height: 1em; padding-right: 6px;">എ</span><br />
ന്നിലെ ഹരിതാഭ-<br />
യെന്നിലെ ഋതുശോഭ-<br />
യെന്നിലെ പരകായ<br />
യോഗങ്ങളും</p>
<p><span style="float: left; font-size: 700%; line-height: 1em; padding-right: 6px;">എ</span><br />
ൻ പുരാവൃത്തങ്ങ-<br />
ളെൻ പ്രഭാവലയങ്ങ-<br />
ളെന്നുള്ളിലൂറുന്ന<br />
ചോദനയും</p>
<p><span style="float: left; font-size: 700%; line-height: 1em; padding-right: 6px;">എ</span><br />
ന്റെ കിനാവിലി-<br />
ന്നെന്റെ നിലാവിലി-<br />
ന്നെന്നെ തിരയുന്ന<br />
കാമിനിയും</p>
<p><span style="float: left; font-size: 700%; line-height: 1em; padding-right: 6px;">എ</span><br />
ന്നുള്ളിൽ നിന്നിവ-<br />
യെന്നേ മറഞ്ഞുപോ-<br />
യെന്നുടെ പ്രാണനെ<br />
ഒറ്റയാക്കി</p>
<p><span style="float: left; font-size: 700%; line-height: 1em; padding-right: 6px;">എ</span><br />
ന്നുമറിയുന്നു ഞാ-<br />
നെന്നുമോർക്കുന്നു ഞാ-<br />
നെന്നെന്നുമെന്നെന്നും<br />
ഞാൻ തനിയെ...</p>
