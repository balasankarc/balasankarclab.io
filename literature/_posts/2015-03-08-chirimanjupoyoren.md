---
layout: literature_post
status: publish
permalink: /literature/:title.html
published: true
title: "ചിരിമാഞ്ഞു പോയൊരെൻ..."
author:
  display_name: "ബാലശങ്കർ സി"
  login: admin
  email: c.balasankar@gmail.com
  url: ''
date: '2015-03-08 03:06:02 -0400'
categories:
- Literature
tags: []
comments: []
---
സൗഹൃദം എന്നതിനു് ഞാൻ സങ്കൽപ്പിച്ചു കൂട്ടിയിരുന്നു, ഒരായിരം അർത്ഥഭേദങ്ങൾ.. മറ്റേതൊരു ബന്ധത്തേക്കാളും മഹത്വമുള്ളതായും അതിനെ കരുതിയിരുന്നു.. പക്ഷേ, എല്ലായിടത്തേയും പോലെ, ഏറ്റവും നന്മ നിറഞ്ഞ സൗഹൃദം തന്നെയാണ് എപ്പോഴും ഏറ്റവും വേദന നൽകുന്നതും എന്ന് മനസ്സിലാക്കുമ്പോൾ, ഒരു നഷ്ടപ്പെടലിന്റെ മൊത്തം പൊള്ളലും അതിനുണ്ട്.. നഷ്ടപ്പെടുക എന്ന വാക്ക് തെറ്റാണ്.. സൗഹൃദം കൂടുതൽ കൃത്യമായി നിർവചിക്കപ്പെടുകയും അതിരുകൾക്ക് അകത്തേക്ക് നിശ്ചയിക്കപ്പെടുകയുമാണ്.. അത് ഒരിക്കലും ഒരു തെറ്റോ, മോശം കാര്യമോ അല്ല.. കാലം മറ്റെന്തിനും വ്യക്തത വരുത്തുന്നതു പോലെ സൗഹൃദത്തിനേയും കൂടുതൽ വ്യക്തമാക്കി തരുന്നു എന്നേയുള്ളൂ.. പക്ഷേ, സൗഹൃദത്തിൽ അധിഷ്ഠിതമായി ഒരു വ്യക്തിത്വം രൂപപ്പെടുത്തിയവർക്ക് സൗഹൃദം എന്നത് എന്തിനും പോന്ന ഒരു അത്താണി എന്നതിൽ നിന്ന് ഒരു നിർവചനത്തിന്റെ ചട്ടക്കൂടിലേക്ക് ഒതുങ്ങുമ്പോൾ ബാക്കിയാവുന്നത് രണ്ടേ രണ്ട് കാര്യമാണ്.. ശൂന്യതയും, ഇനിയെന്ത് എന്ന ചോദ്യവും.. ജീവിതം എന്ന കാൻവാസ് മുഴുവൻ നിറഞ്ഞിരുന്ന ഒന്ന്, ഒരു ഡെഫിനിഷന്റെ വെൻ ഡയഗ്രത്തിലേക്ക് ഒതുങ്ങുമ്പോൾ ബാക്കിയാവുന്ന വാക്വം ഭീതിദമാണ്.. ഇനിയുള്ള അസ്തിത്വത്തെ തന്നെ ചോദ്യം ചെയ്യാൻ പ്രേരിപ്പിക്കുന്ന ഒന്ന്.. നിഷ്കളങ്കമായ സൗഹൃദം എന്നതിൽ നിന്ന് പക്വമായ സൗഹൃദം എന്നതിലേക്കുള്ള മാറ്റം, മറ്റേത് പറിച്ചുനടലിനേക്കാളും വേദനാജനകം ആണു്.. അതിനെ പറിച്ചുനടലിനേക്കാളുപരി ഒരു അപ്ഡേഷൻ ആയി വേണം കാണാൻ എന്നറിയാമായിരുന്നിട്ടും, അത് കണ്ണീർ മാത്രമാണ് നൽകുന്നത്.. പ്രജ്ഞയുടെ ഒരു പാതി യാഥാർത്ഥ്യത്തെ സ്വീകരിക്കണമെന്ന് യുക്തിപൂർവ്വം നിർബന്ധിക്കുമ്പോഴും, ലോകനീതി എന്നതിന്റെ കളിക്ക് വിട്ടുകൊടുക്കാവുന്നതിലും വിലപ്പെട്ടതാണ് ആ സൗഹൃദം എന്ന് നിർത്താതെ ഓർമ്മിപ്പിക്കുന്നു മറുപാതി.. ആരൊക്കെ എങ്ങനെയൊക്കെ ന്യായീകരിച്ചാലും, പക്വത എന്നത് നിഷ്കളങ്കതയുടെ ചുടലപ്പറമ്പാണ്.. അതിനാൽ തന്നെ നിഷ്കളങ്ക സൗഹൃദത്തിന്റേയും..
