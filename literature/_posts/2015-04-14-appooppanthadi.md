---
layout: literature_post
status: publish
permalink: /literature/:title.html
published: true
title: "അപ്പൂപ്പൻതാടി"
author:
  display_name: "ബാലശങ്കർ സി"
  login: admin
  email: c.balasankar@gmail.com
  url: ''
date: '2015-04-14 18:47:36 -0400'
categories:
- Literature
tags: []
comments: []
---

![]({{"/literature/images/appooppanthadi.png" | prepend: site.baseurl}}){:width="100%"}

ചിത്രത്തിനു് കടപ്പാട് : ശ്രീജിത്ത് കെ, വിക്കിപീഡിയ
