---
layout: literature_post
status: publish
permalink: /literature/:title.html
published: true
title: Mind @ a midnight
author:
  display_name: "ബാലശങ്കർ സി"
  login: admin
  email: c.balasankar@gmail.com
  url: ''
date: '2013-06-28 20:40:38 -0400'
categories:
- Literature
---

Poison rising through my veins  
Mind floating like those kites  
Thoughts running with broken reigns  
and ending like those little flies  
Tonight I grieve, tonight I weep  
Tonight I see mu lies in heap  
Out in the rain or under the shade  
I feel like being Zeus or Hades  
Half being devil n half being love  
It seems my life is pulled by a tow  
Lost my dreams and the stars in them  
The smell of a rose n glitter of a gem  
Parted from life, let out a fart  
Parted from love, shed down ma heart...  
