---
layout: literature_post
status: publish
permalink: /literature/:title.html
published: true
title: "ജനൽമഴ"
author:
  display_name: "ബാലശങ്കർ സി"
  login: admin
  email: c.balasankar@gmail.com
  url: ''
date: '2015-04-22 21:36:12 -0400'

categories:
- Literature
tags: []
comments: []
---

![]({{"/literature/images/mazha.jpg" | prepend: site.baseurl}}){:width="100%"}

ചിത്രത്തിനു് കടപ്പാട് : [n8foo - flickr](https://www.flickr.com/photos/n8foo/7607869358)
