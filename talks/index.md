---
layout: default
title: Talks and Workshops
permalink: /talks/
---

<style>
.timeline > h1 {}
</style>

# Talks and Workshops

{:.bodytext}
I occasionally give talks and workshops on various technical topics.

<div class="timeline">
  {% for item in site.data.talks %}
  <h1 id="{{ item.year }}">{{ item.year }}</h1>
  <ul>
  {% for talk in item.talks %}
  {% if talk.title %}
  <li><h2>{{ talk.title}}</h2></li>
  <p class="bodytext-larger">
    {% if talk.month %} {{talk.month}} {% endif %}
    {% if talk.event.name %} |
      {% if talk.event.url %}<a href="{{talk.event.url}}">{% endif %}{{talk.event.name}}{% if talk.event.url %}</a>{% endif %}
    {% endif %}
    {% if talk.event.place %}, {{talk.event.place}} {% endif %}
  </p>
  {% endif %}
  {% endfor %}
  </ul>
  {% endfor %}
</div>
