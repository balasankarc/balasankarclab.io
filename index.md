---
layout: home
---

# About

My name is Balasankar C, but I usually go by "Balu" almost everywhere. I am a
programmer, originally from [Kalady, Kerala](https://www.openstreetmap.org/node/594969055),
and currently lives in [Bengaluru, Karnataka](https://www.openstreetmap.org/node/3401391999).

I work at [GitLab](https://about.gitlab.com) as a Senior Software Engineer on their
[Distribution team](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/).
Other major identities I have are
[Debian Developer](https://portfolio.debian.net/result?email=balasankarc%40debian.org),
[Mozillian](https://mozillians.org/en-US/u/balasankarc/),
[GNOME Foundation Member (Emeritus)](https://wiki.gnome.org/MembershipCommittee/EmeritusMembers#List_of_Emeritus_Members),
[Administrator of Free Software Community of India](https://fsci.org.in),
[Administrator of Malayalam Wikisource](https://ml.wikisource.org/wiki/Wikisource:Administrators).

It may be clear from my above identities that I am pretty much
interested in the Free Software ideology and organizations around it. Other than
that, I am also a privacy advocate who is interested in the social perspectives
of individual privacy and the technology surrounding it. I attended and
[completed](https://balasankarc.in/tech/category/GSoC/) Google Summer of Code in
2016 under [Indic Project](https://indicproject.org) on their [Malayalam spellchecker project](https://github.com/libindic/spellchecker).

I am also a self-proclaimed [Bibliophile](https://www.goodreads.com/balasankarc)
and a [Procaffeinator](http://procaffination.urbanup.com/7543574), and identify
myself as a [textrovert](http://textrovert.urbanup.com/3137603) (however, if the
topic is something tech related, I don't mind verbal talks/debates/screaming
matches etc.). I am happy to mentor people on topics that I am confident on, and
I occasionally give [talks](/talks) on various technical topics I am interested
in.

# Finding me on the Internet

You can find me at various social media and other discussion platforms on the
Internet. The major ones that I usually check are

* Mastodon: [balu@dravidam.net](https://mastodon.dravidam.net/@balu){:rel="me"}
* Twitter: [@balasankarc](https://twitter.com/balasankarc)
* GitLab: [@balasankarc](https://gitlab.com/balasankarc)
* Diaspora: [bsc@poddery.com](https://poddery.com/u/bsc)

And no, I am [not on Facebook](https://balasankarc.in/tech/goodbye-facebook.html).

# Blogs

On the rare occasions when I decide to write something, and actually end up
doing it, you can find them at my blog, [Technocrazian](/tech). I used to
dabble in literature in the past, of which I am quite embarassed these days. If
you insist, you can find them at [ശങ്കരാഭരണം](/literature)

# Contact Me

I am notorious for not being available over phone, and prefer text-based
communication more. You can probably reach me via the following

* EMail: mail [AT] balasankarc [dot] in
* Matrix: @balu:dravidam.net
* Telegram: [@balasankarc](https://t.me/balasankarc)

Whenever possible, I prefer encrypted communication.
My PGP key is `DDF2B83B163533E05B696D5BB77D2E2E23735427`.
