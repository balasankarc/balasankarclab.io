---
layout: post
status: publish
permalink: /tech/:title.html
published: true
title: 'FOSSAsia 2018 - Singapore'
author:
  display_name: Balasankar C
  login: admin
  email: balasankarc@autistici.org
  url: ''
categories:

---

Heyo,

So I attended my first international FOSS conference - [FOSSAsia 2018](http://2018.fossasia.org/)
at [Lifelong learning institute, Singapore](https://www.lli.sg/). I presented a
talk titled "Omnibus - Serve your dish on all the tables"
([slides](/talks/fossasia), [video](https://www.youtube.com/watch?v=8P1gokiEhT0))
about the tool Chef Omnibus which I use on a daily basis for my job at
[GitLab](https://about.gitlab.com).

The conference was a 4-day long one and my main aim was to network with as many
people as I can. Well, I planned to attend sessions, but unlike earlier times
when I attended all the sessions, these days I am more focussed on certain
topics and technologies and tend to attend sessions on those (for example,
devops is an area I focuses on, block chain isn't).

One additional task I had was attend the Debian booth at the exhibition from
time to time. It was mainly handled by Abhijith (who is a DM). I also met two
other Debian Developers there - [Andrew Lee](https://wiki.debian.org/AndrewLee)(alee)
and [Héctor Orón Martínez](https://wiki.debian.org/HectorOron)(zumbi).

I also met some other wonderful people at FOSSAsia, like Chris Aniszczyk of
CNCF, Dr Graham Williams of Microsoft, Frank Karlitschek of NextCloud,
Jean-Baptiste Kempf and Remi Denis-Courmont of VideoLan, Stephanie Taylor of
Google, Philip Paeps(trouble) of FreeBSD, Harish Pillai of RedHat, Anthony,
Christopher Travers, Vasudha Mathur of KDE, Adarsh S of CloudCV (and who is from
MEC College, which is quite familiar to me), Tarun Kumar of Melix, Roy Peter of
Go-Jek (with whom I am familiar, thanks to the Ruby conferences I attended),
Dias Lonappan of Serv and many more.  I also met with some whom I know knew only
digitally, like Sana Khan who was (yet another, :D) a Debian contributor from
COEP. I also met with some friends like Hari, Cherry, Harish and Jackson.

My talk went ok without too much of stuttering and I am kinda satisfied by it.
The only thing I forgot is to mention during the talk that I had stickers (well,
I later placed them in the sticker table and it disappeared within minutes. So
that was ok. ;))

PS: Well, I had to cut down quite a lot of my explanation and drop my demo due
to limited time. This caused me miss many important topics like omnibus-ctl or
cookbooks that we use at GitLab. But, I had a few participants come up and meet
me after the talk, with doubts regarding omnibus and its similarity with
flatpak, relevance during the times of Docker etc, which was good.

Some photos are here:

![Abhijith in Debian Booth]({{"/images/tech/fossasia/Abhijith.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">Abhijith in Debian Booth</div>

![Abhijith with VLC folks]({{"/images/tech/fossasia/Abhijith_VLC.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">Abhijith with VLC folks</div>

![Andrew's talk]({{"/images/tech/fossasia/alee.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">Andrew's talk</div>

![With Anthony and Harish: Two born-and-brought-up-in-SG-Malayalees]({{"/images/tech/fossasia/Anthony_Harish.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">With Anthony and Harish: Two born-and-brought-up-in-SG-Malayalees</div>

![Chris Aniszczyk]({{"/images/tech/fossasia/Chris.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">With Chris Aniszczyk</div>

![Debian Booth]({{"/images/tech/fossasia/debian-booth.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">At Debian Booth</div>

![Frank Karlitschek]({{"/images/tech/fossasia/Frank.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">With Frank Karlitschek</div>

![Graham Williams]({{"/images/tech/fossasia/Graham.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">With Graham Williams</div>

![MOS Burgers - Our breakfast place]({{"/images/tech/fossasia/MOS.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">MOS Burgers - Our breakfast place</div>

![Premas Cuisine - The kerala taste]({{"/images/tech/fossasia/Premas.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">Premas Cuisine - The kerala taste</div>

![The joy of seeing Malayalam]({{"/images/tech/fossasia/Premas_ML.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">The joy of seeing Malayalam</div>

![With Sana]({{"/images/tech/fossasia/sana.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">With Sana</div>

![Well, Tamil, ftw]({{"/images/tech/fossasia/Tamil.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">Well, Tamil, ftw</div>

![Zumbi's talk]({{"/images/tech/fossasia/zumbi.jpg" | prepend: site.baseurl}}){: .inline}
<div class="caption">Zumbi's talk</div>
