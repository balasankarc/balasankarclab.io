---
layout: post
permalink: /tech/:title.html
title: '4 Days. 22 Hours. LaTeX.'
author:
  display_name: Balasankar C
  login: admin
  email: mail@balasankarc.in
  url: ''
categories:

- Lecture
tags:
- latex
- training
- teaching
- knowlege
---

Heyo folks,

One of the stuff I love doing is teaching what I know to others. Though it is a Cliché dialogue, I know from experience that when we teach others our knowledge expands. From 10 students, you often get 25 different doubts and minimum 5 of them would be ones you haven't even thought yourself earlier. In that way, teaching drives a our curiosity to find out more.
<!--more-->

I was asked to take a LaTeX training for B.Tech students as a bridge course (happening during their semester breaks. Poor kids!). The usual scenario is faculty taking class and we PG students assisting them. But, since all the faculty members were busy with their own subjects' bridge courses and LaTeX was something like an additional skill that the students need for their next semesters for their report preparation, I was asked to take to take it with the assistance of my classmates. At first, I was asked to take a two-day session for third year IT students. But later, HOD decided that both CS and IT students should have that class, and guess what - I had to teach for four days. Weirdly, the IT class was split to two non-continuous dates - Monday and Wednesday. So, I didn't have to take class for four consecutive days, but only three. :D

The syllabus I followed is as follows:

 * Basic LaTeX – Session I
    1. Brief introduction about LaTeX, general document structure, packages etc.
    2. Text Formatting
    3. Lists – Bullets and Numbering
 * Graphics and Formulas – Session II
    1. Working with Images
    2. Tables
    3. Basic Mathematical Formulas
 * Academic Document Generation (Reports and Papers) – Session III
    1. Sectioning and Chapters
    2. Header and Footer
    3. Table of Contents
    4. Adding Bibliography and Citations
    5. IEEETran template
 * Presentations using Beamer – Session IV

As (I, not the faculty) expected, only half of the students came (Classes on semester breaks, I was surprised when even half came!). Both the workshops - for CS and IT - were smooth without any much issues or hinderences. Students didn't hesitate much to ask doubts or tips on how to do stuff that I didn't teach (Unfortunately, I didn't have time to go off-syllabus, so I directed them to Internet. :D). Analysing the students, CS students were more lively and interactive but they took some time to grasp the concept. Compared to them, even though kind of silent, IT students learned stuff fast.

By Friday, I had completed 4 days, around 22 hours of teaching and that too non-stop. I was tired each day after the class, but it was still fun to share the stuff I know. I would love to get this chance again.

![IT Batch]({{"/images/tech/latex-it.jpeg" | prepend: site.baseurl}})

<div class="caption">IT Batch</div>
<br />
<br />
<br />
![CSE Batch]({{"/images/tech/latex-cs.jpeg" | prepend: site.baseurl}})

<div class="caption">CSE Batch</div>
