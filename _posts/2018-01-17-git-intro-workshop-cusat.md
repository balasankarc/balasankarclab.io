---
layout: post
status: publish
permalink: /tech/:title.html
published: true
title: 'Introduction to Git workshop at CUSAT'
author:
  display_name: Balasankar C
  login: admin
  email: balasankarc@autistici.org
  url: ''
categories:

tags:
comments: []
---

Heyo,

It has been long since I have written somewhere. In the last year I attended some events, like FOSSMeet, DeccanRubyConf, GitLab's summit and didn't write anything about it. The truth is, I forgot I used to write about all these and never got the motivation to do that.

Anyway, last week, I conducted a workshop on Git basics for the students of [CUSAT](http://cusat.ac.in/). My real plan, as always, was to do a bit of FOSS evangelism too. Since the timespan of workshop was limited (10:00 to 13:00), I decided to keep everything to bare basics.

Started with an introduction to what a VCS is and how it became necessary. As a prerequisite, I talked about FOSS, concept of collaborative development, open source development model etc. It wasn't easy as my audience were not only CS/IT students, but those from other departments like Photonics, Physics etc. I am not sure if I was able to help them understand the premise clearly. However, then I went on to talk about what Git does and how it helps developers across the world.

IIRC, this was the first talk/workshop I did without a slide show. I was damn lazy and busy to create one. I just had one page saying "Git Workshop" and my contact details. So guess what? I used a whiteboard! I went over the basic concepts like repositories, commits, staging area etc and started with the hand-on session. In short, I talked about the following
1. Initializing a repository
2. Adding files to it
3. Add files to staging areas
4. Committing
5. Viewing commit logs
6. Viewing what a specific commit did
7. Viewing a file's contents at a specific commit
8. Creating a GitLab account (Well, use all opportunity to talk about your employer. :P)
9. Creating a project in GitLab
10. Adding it as a remote repository to your local one
11. Pushing your changes to remote repository

I wanted to talk about clone, fork, branch and MRs, but time didn't permit. We wound up the session with Athul and Kiran talking about how they need the students to join the FOSSClub of CUSAT, help organizing similar workshops and how it can help them as well. I too did a bit of "motivational talk" regarding how community activities can help them get a job, based on my personal experience.

Here are a few photos, courtesy of Athul and Kiran:

![]({{"/images/tech/cusat-git/image_1.jpg" | prepend: site.baseurl}}){: .inline}

![]({{"/images/tech/cusat-git/image_2.jpg" | prepend: site.baseurl}}){: .inline}

![]({{"/images/tech/cusat-git/image_3.jpg" | prepend: site.baseurl}}){: .inline}

![]({{"/images/tech/cusat-git/image_4.jpg" | prepend: site.baseurl}}){: .inline}
