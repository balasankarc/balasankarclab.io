---
layout: post
status: publish
permalink: /tech/:title.html
published: true
title: 'SMC/IndicProject Activities- ToDo List'
author:
  display_name: Balasankar C
  login: admin
  email: balasankarc@autistici.org
  url: ''
date: '2016-09-07 00:47:17 -0400'
categories:

- SMC
tags:
- foss
- smc
- indicproject
- community
comments: []
---
Heyo,

So, M.Tech is coming to an end I should probably start searching for a job soon. Still, it seems I will be having a bit of free time from Mid-September. I have got some plans about the areas I should contribute to SMC/Indic Project. As of now, the bucket list is as follows:
<!--more-->

 1. **Properly tag versions of fonts in SMC GitLab repo** - I had taken over the package fonts-smc from Vasudev, but haven't done any update on that yet. The main reason was fontforge being old in Debian. Also, I was waiting for some kind of official release of new versions by SMC. Since the new versions are already available in the SMC Fonts page, I assume I can go ahead with my plans. So, as a first step I have to tag the versions of fonts in the corresponding GitLab repo. Need to discuss whether to include TTF file in the repo or not.
 2. **Restructure LibIndic modules** - Those who were following my [GSoC posts](http://localhost:4000/tech/category/GSoC/) will know that I made some structural changes to the modules I contributed in LibIndic. (Those who don't can check [this mail](http://lists.nongnu.org/archive/html/silpa-discuss/2016-08/msg00006.html) I sent to the list). I plan to do this for all the modules in the framework, and to co-ordinate with Jerin to get REST APIs up.
 3. **GNOME Localization** - [GNOME Localization](https://l10n.gnome.org/teams/ml/) has been dead for almost two years now. Ashik has shown interest in re-initiating it and I plan to do that. I first have to get my committer access back.
 4. **Documentation** - Improve documentation about SMC and IndicProject projects. This will be a troublesome and time consuming task but I still like our tools to have proper documentation.
 5. **High Priority Projects** - Create a static page about the high priority projects so that people can know where and how to contribute.
 6. **Die Wiki, Die** - Initiate porting Wiki to a static site using Git and Jekyll (or any similar tool). Tech people should be able to use git properly.

Knowing me pretty much better than anyone else, I understand there is every chance of this being "Never-being-implemented-plan" (അതായത് ആരംഭശൂരത്വം :D) but still I intend to do this in an easy-first order.
