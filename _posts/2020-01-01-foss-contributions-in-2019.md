---
layout: post
status: publish
permalink: /tech/:title.html
title: 'FOSS contributions in 2019'
categories:
- FOSS
---

Heyo,

I have been interested in the concept of Freedom - both in the technical and
social ecosystems for almost a decade now. Even though I am not a harcore
contributor or anything, I have been involved in it for few years now - as an
enthusiast, a contributor, a mentor, and above all an evangelist. Since 2019 is
coming to an end, I thought I will note down what all I did last year as a FOSS
person.

## GitLab

My job at GitLab is that of a Distribution Engineer. In simple terms, I have to
deal with anything that a user/customer may use to install or deploy GitLab. My
team maintains the omnibus-gitlab packages for various OSs, docker image, AWS
AMIs and Marketplace listings, Cloud Native docker images, Helm charts for
Kubernetes, etc.

My job description is essentially dealing with the above mentioned tasks
only, and as part of my day job I don't usually have to write and backend
Rails/Go code. However, I also find GitLab as a good open source project and
have been contributing few features to it over the year. Few main reasons I
started doing this are
1. An opportunity to learn more Rails. GitLab is a pretty good project to do
   that, from an engineering perspective.
1. Most of the features I implemented are the ones I wanted from GitLab, the
   product. The rest are technically simpler issues with less complexity(relates
   to the point above, regarding getting better at Rails).
1. I know the never-ending dilemma our Product team goes through to always
   maintain the balance of CE v/s EE features in every release, and prioritizing
   appropriate issues from a mountain of backlog to be done on each milestone.
   In my mind, it is easier for both them and me if I just implemented something
   rather than asked them to schedule it to be done by a backend team, so that I
   cane enjoy the feature. To note, most of the issues I tackled already had
   `Accepting Merge Requests` label on them, which meant Product was in
   agreement that the feature was worthy of having, but there were issues with
   more priority to be tackled first.

So, here are the features/enhancements I implemented in GitLab, as an interested
contributor in the selfish interest of improving my Rails understanding and to
get features that I wanted without much waiting:

1. [Add number of repositories to usage ping data](https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/24823)
1. [Provide an API endpoint to get GPG signature of a commit](https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/25032)
1. [Add ability to set project path and name when forking a project via API](https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/25363)
1. [Add predefined CI variable to provide GitLab FQDN](https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/30417)
1. [Ensure changelog filenames have less than 99 characters](https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/31752)
1. [Support notifications to be fired for protected branches also](https://gitlab.com/gitlab-org/gitlab/merge_requests/16405)
1. [Set X-GitLab-NotificationReason header in emails that are sent due to explicit subscription to an issue/MR](https://gitlab.com/gitlab-org/gitlab/merge_requests/18812)
1. [Truncate recommended branch name to a sane length](https://gitlab.com/gitlab-org/gitlab/merge_requests/18821)
1. [Support passing CI variables as push options](https://gitlab.com/gitlab-org/gitlab/merge_requests/20255)
1. [Add option to configure branches for which emails should be sent on push](https://gitlab.com/gitlab-org/gitlab/merge_requests/22196)

## Swathanthra Malayalam Computing

I have been a volunteer at Swathanthra Malayalam Computing for almost 8 years
now. Most of my contributions are towards various localization efforts that SMC
coordinates. Last year, my major contributions were improving our fonts build
process to help various packaging efforts (well, selfish reason - I wanted my
life as the maintainer of Debian packages to be easier), implementing CI based
workflows for various projects and helping in evangelism.

1. [Ensuring all our fonts build with Python3](https://gitlab.com/smc/smc-project/issues/98)
1. [Ensuring all our fonts have proper appstream metadata files](https://gitlab.com/smc/smc-project/issues/93)
1. [Add an FAQ page to Malayalam Speech Corpus](https://gitlab.com/smc/msc/merge_requests/2)
1. [Add release workflow using CI for Magisk font module](https://gitlab.com/smc/magisk-malayalam-fonts/merge_requests/1)

## Debian

I have been a Debian contributor for almost 8 years, became a Debian Maintainer
3 years after my first stint with Debian, and have been a Debian Developer for 2
years. My activities as a Debian contributor this year are:

1. Continuing maintenance of `fonts-smc-*` and `hyphen-indic` packages.
1. [Packaging](https://wiki.debian.org/Teams/DebianGoTeam/GopasspwPackaging) of
   [gopass password manager](https://github.com/gopasspw/gopass/). This has been
   going on very slow.
1. [Reviewing and sponsoring various Ruby and Go packages](https://qa.debian.org/developer.php?login=Balasankar%20C%20%3Cbalasankarc@debian.org%3E&comaint=yes#Sponsored/other%20uploads).
1. Help GitLab packaging efforts, both as a Debian Developer and a GitLab
   employee.

## Other FOSS projects

In addition to the main projects I am a part of, I contributed to few FOSS last
year, either due to personal interest, or as part of my job. They are:

1. [Calamares](https://calamares.io) - I [initiated](https://euroquis.nl//calamares/2019/09/24/malayalam.html)
   and [spearheaded](https://euroquis.nl/bla%20bla/2019/10/22/potpourri.html#calamares)
   the localization of Calamares installer to [Malayalam language](https://www.transifex.com/calamares/calamares/language/ml/).
   It reached 100% translated status within a month.
1. [Chef](https://www.chef.io/)
     1. [Fix openSUSE Leap and SLES detection in Chef Ohai 14](https://github.com/chef/ohai/pull/1377)
     1. [Make runit service's control commands configurable in Chef Runit cookbook](https://github.com/chef-cookbooks/runit/pull/243)
1. [Mozilla](https://mozilla.org/) - Being one of the Managers for Malayalam
   Localization team of Mozilla, I helped coordinate localizations of various
   projects, interact with Mozilla staff for the community in clarifying their
   concerns, getting new projects added for localization etc.

## Talks

I also gave few talks regarding various FOSS topics that I am
interested/knowledgeable in during 2019. List and details can be found at the
[talks](/talks) page.

Overall, I think 2019 was a good year for the FOSS person in me. Next year, I
plan to be more active in Debian because from the above list I think that is
where I didn't contribute as much as I wanted.
